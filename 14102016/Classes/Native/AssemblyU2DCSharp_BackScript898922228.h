﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// Purchaser
struct Purchaser_t1507804203;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackScript
struct  BackScript_t898922228  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject BackScript::ParentCirner
	GameObject_t1756533147 * ___ParentCirner_2;
	// UnityEngine.GameObject BackScript::Slidec
	GameObject_t1756533147 * ___Slidec_3;
	// UnityEngine.GameObject BackScript::SldieE
	GameObject_t1756533147 * ___SldieE_4;
	// UnityEngine.GameObject BackScript::threemenu
	GameObject_t1756533147 * ___threemenu_5;
	// UnityEngine.GameObject BackScript::membershipdetails
	GameObject_t1756533147 * ___membershipdetails_6;
	// UnityEngine.GameObject BackScript::purch
	GameObject_t1756533147 * ___purch_7;
	// Purchaser BackScript::pur
	Purchaser_t1507804203 * ___pur_8;

public:
	inline static int32_t get_offset_of_ParentCirner_2() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___ParentCirner_2)); }
	inline GameObject_t1756533147 * get_ParentCirner_2() const { return ___ParentCirner_2; }
	inline GameObject_t1756533147 ** get_address_of_ParentCirner_2() { return &___ParentCirner_2; }
	inline void set_ParentCirner_2(GameObject_t1756533147 * value)
	{
		___ParentCirner_2 = value;
		Il2CppCodeGenWriteBarrier(&___ParentCirner_2, value);
	}

	inline static int32_t get_offset_of_Slidec_3() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___Slidec_3)); }
	inline GameObject_t1756533147 * get_Slidec_3() const { return ___Slidec_3; }
	inline GameObject_t1756533147 ** get_address_of_Slidec_3() { return &___Slidec_3; }
	inline void set_Slidec_3(GameObject_t1756533147 * value)
	{
		___Slidec_3 = value;
		Il2CppCodeGenWriteBarrier(&___Slidec_3, value);
	}

	inline static int32_t get_offset_of_SldieE_4() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___SldieE_4)); }
	inline GameObject_t1756533147 * get_SldieE_4() const { return ___SldieE_4; }
	inline GameObject_t1756533147 ** get_address_of_SldieE_4() { return &___SldieE_4; }
	inline void set_SldieE_4(GameObject_t1756533147 * value)
	{
		___SldieE_4 = value;
		Il2CppCodeGenWriteBarrier(&___SldieE_4, value);
	}

	inline static int32_t get_offset_of_threemenu_5() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___threemenu_5)); }
	inline GameObject_t1756533147 * get_threemenu_5() const { return ___threemenu_5; }
	inline GameObject_t1756533147 ** get_address_of_threemenu_5() { return &___threemenu_5; }
	inline void set_threemenu_5(GameObject_t1756533147 * value)
	{
		___threemenu_5 = value;
		Il2CppCodeGenWriteBarrier(&___threemenu_5, value);
	}

	inline static int32_t get_offset_of_membershipdetails_6() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___membershipdetails_6)); }
	inline GameObject_t1756533147 * get_membershipdetails_6() const { return ___membershipdetails_6; }
	inline GameObject_t1756533147 ** get_address_of_membershipdetails_6() { return &___membershipdetails_6; }
	inline void set_membershipdetails_6(GameObject_t1756533147 * value)
	{
		___membershipdetails_6 = value;
		Il2CppCodeGenWriteBarrier(&___membershipdetails_6, value);
	}

	inline static int32_t get_offset_of_purch_7() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___purch_7)); }
	inline GameObject_t1756533147 * get_purch_7() const { return ___purch_7; }
	inline GameObject_t1756533147 ** get_address_of_purch_7() { return &___purch_7; }
	inline void set_purch_7(GameObject_t1756533147 * value)
	{
		___purch_7 = value;
		Il2CppCodeGenWriteBarrier(&___purch_7, value);
	}

	inline static int32_t get_offset_of_pur_8() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___pur_8)); }
	inline Purchaser_t1507804203 * get_pur_8() const { return ___pur_8; }
	inline Purchaser_t1507804203 ** get_address_of_pur_8() { return &___pur_8; }
	inline void set_pur_8(Purchaser_t1507804203 * value)
	{
		___pur_8 = value;
		Il2CppCodeGenWriteBarrier(&___pur_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
