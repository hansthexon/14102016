﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Ball_anim
struct Ball_anim_t472390881;

#include "codegen/il2cpp-codegen.h"

// System.Void Ball_anim::.ctor()
extern "C"  void Ball_anim__ctor_m1235810110 (Ball_anim_t472390881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_anim::Start()
extern "C"  void Ball_anim_Start_m2686335398 (Ball_anim_t472390881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_anim::Update()
extern "C"  void Ball_anim_Update_m2413376105 (Ball_anim_t472390881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Ball_anim::ballAnimate()
extern "C"  void Ball_anim_ballAnimate_m417180686 (Ball_anim_t472390881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
