﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Checkandload
struct Checkandload_t1170646197;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Checkandload::.ctor()
extern "C"  void Checkandload__ctor_m4108573804 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::Start()
extern "C"  void Checkandload_Start_m3822983476 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::Update()
extern "C"  void Checkandload_Update_m922599837 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Checkandload::Download()
extern "C"  Il2CppObject * Checkandload_Download_m3235932686 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Checkandload::dowloadVideo(System.String)
extern "C"  Il2CppObject * Checkandload_dowloadVideo_m1430225391 (Checkandload_t1170646197 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::UpdateDb(System.String)
extern "C"  void Checkandload_UpdateDb_m3853772321 (Checkandload_t1170646197 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::Updatemytest(System.String)
extern "C"  void Checkandload_Updatemytest_m3196761833 (Checkandload_t1170646197 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::nextscene()
extern "C"  void Checkandload_nextscene_m3340925345 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::loadlevelwith()
extern "C"  void Checkandload_loadlevelwith_m779204178 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload::IncreaseBar()
extern "C"  void Checkandload_IncreaseBar_m872154225 (Checkandload_t1170646197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Checkandload::LoadLevelWithBar(System.Int32)
extern "C"  Il2CppObject * Checkandload_LoadLevelWithBar_m4039859144 (Checkandload_t1170646197 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
