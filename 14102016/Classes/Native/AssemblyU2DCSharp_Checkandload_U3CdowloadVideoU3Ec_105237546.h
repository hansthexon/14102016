﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Object
struct Il2CppObject;
// Checkandload
struct Checkandload_t1170646197;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Checkandload/<dowloadVideo>c__Iterator6
struct  U3CdowloadVideoU3Ec__Iterator6_t105237546  : public Il2CppObject
{
public:
	// System.String Checkandload/<dowloadVideo>c__Iterator6::filename
	String_t* ___filename_0;
	// UnityEngine.WWW Checkandload/<dowloadVideo>c__Iterator6::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.Int32 Checkandload/<dowloadVideo>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object Checkandload/<dowloadVideo>c__Iterator6::$current
	Il2CppObject * ___U24current_3;
	// System.String Checkandload/<dowloadVideo>c__Iterator6::<$>filename
	String_t* ___U3CU24U3Efilename_4;
	// Checkandload Checkandload/<dowloadVideo>c__Iterator6::<>f__this
	Checkandload_t1170646197 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(U3CdowloadVideoU3Ec__Iterator6_t105237546, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier(&___filename_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CdowloadVideoU3Ec__Iterator6_t105237546, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CdowloadVideoU3Ec__Iterator6_t105237546, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CdowloadVideoU3Ec__Iterator6_t105237546, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efilename_4() { return static_cast<int32_t>(offsetof(U3CdowloadVideoU3Ec__Iterator6_t105237546, ___U3CU24U3Efilename_4)); }
	inline String_t* get_U3CU24U3Efilename_4() const { return ___U3CU24U3Efilename_4; }
	inline String_t** get_address_of_U3CU24U3Efilename_4() { return &___U3CU24U3Efilename_4; }
	inline void set_U3CU24U3Efilename_4(String_t* value)
	{
		___U3CU24U3Efilename_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efilename_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CdowloadVideoU3Ec__Iterator6_t105237546, ___U3CU3Ef__this_5)); }
	inline Checkandload_t1170646197 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline Checkandload_t1170646197 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(Checkandload_t1170646197 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
