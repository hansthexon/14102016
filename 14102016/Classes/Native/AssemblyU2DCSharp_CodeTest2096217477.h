﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeTest
struct  CodeTest_t2096217477  : public Il2CppObject
{
public:
	// System.String CodeTest::CodeARimage
	String_t* ___CodeARimage_0;
	// System.String CodeTest::VideoFile
	String_t* ___VideoFile_1;
	// System.String CodeTest::Type
	String_t* ___Type_2;
	// System.String CodeTest::Type2
	String_t* ___Type2_3;
	// System.String CodeTest::PolitePlanet
	String_t* ___PolitePlanet_4;
	// System.String CodeTest::SelfVideoFile
	String_t* ___SelfVideoFile_5;
	// System.String CodeTest::Country
	String_t* ___Country_6;
	// System.String CodeTest::Purchased
	String_t* ___Purchased_7;
	// System.String CodeTest::Content
	String_t* ___Content_8;
	// System.String CodeTest::FirstScore
	String_t* ___FirstScore_9;
	// System.String CodeTest::RescanScore
	String_t* ___RescanScore_10;
	// System.String CodeTest::SelfFirstScore
	String_t* ___SelfFirstScore_11;
	// System.String CodeTest::SelfRescanScore
	String_t* ___SelfRescanScore_12;
	// System.String CodeTest::Version
	String_t* ___Version_13;
	// System.String CodeTest::ForMonth
	String_t* ___ForMonth_14;
	// System.String CodeTest::First
	String_t* ___First_15;
	// System.String CodeTest::SelfFirst
	String_t* ___SelfFirst_16;

public:
	inline static int32_t get_offset_of_CodeARimage_0() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___CodeARimage_0)); }
	inline String_t* get_CodeARimage_0() const { return ___CodeARimage_0; }
	inline String_t** get_address_of_CodeARimage_0() { return &___CodeARimage_0; }
	inline void set_CodeARimage_0(String_t* value)
	{
		___CodeARimage_0 = value;
		Il2CppCodeGenWriteBarrier(&___CodeARimage_0, value);
	}

	inline static int32_t get_offset_of_VideoFile_1() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___VideoFile_1)); }
	inline String_t* get_VideoFile_1() const { return ___VideoFile_1; }
	inline String_t** get_address_of_VideoFile_1() { return &___VideoFile_1; }
	inline void set_VideoFile_1(String_t* value)
	{
		___VideoFile_1 = value;
		Il2CppCodeGenWriteBarrier(&___VideoFile_1, value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___Type_2)); }
	inline String_t* get_Type_2() const { return ___Type_2; }
	inline String_t** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(String_t* value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier(&___Type_2, value);
	}

	inline static int32_t get_offset_of_Type2_3() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___Type2_3)); }
	inline String_t* get_Type2_3() const { return ___Type2_3; }
	inline String_t** get_address_of_Type2_3() { return &___Type2_3; }
	inline void set_Type2_3(String_t* value)
	{
		___Type2_3 = value;
		Il2CppCodeGenWriteBarrier(&___Type2_3, value);
	}

	inline static int32_t get_offset_of_PolitePlanet_4() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___PolitePlanet_4)); }
	inline String_t* get_PolitePlanet_4() const { return ___PolitePlanet_4; }
	inline String_t** get_address_of_PolitePlanet_4() { return &___PolitePlanet_4; }
	inline void set_PolitePlanet_4(String_t* value)
	{
		___PolitePlanet_4 = value;
		Il2CppCodeGenWriteBarrier(&___PolitePlanet_4, value);
	}

	inline static int32_t get_offset_of_SelfVideoFile_5() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___SelfVideoFile_5)); }
	inline String_t* get_SelfVideoFile_5() const { return ___SelfVideoFile_5; }
	inline String_t** get_address_of_SelfVideoFile_5() { return &___SelfVideoFile_5; }
	inline void set_SelfVideoFile_5(String_t* value)
	{
		___SelfVideoFile_5 = value;
		Il2CppCodeGenWriteBarrier(&___SelfVideoFile_5, value);
	}

	inline static int32_t get_offset_of_Country_6() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___Country_6)); }
	inline String_t* get_Country_6() const { return ___Country_6; }
	inline String_t** get_address_of_Country_6() { return &___Country_6; }
	inline void set_Country_6(String_t* value)
	{
		___Country_6 = value;
		Il2CppCodeGenWriteBarrier(&___Country_6, value);
	}

	inline static int32_t get_offset_of_Purchased_7() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___Purchased_7)); }
	inline String_t* get_Purchased_7() const { return ___Purchased_7; }
	inline String_t** get_address_of_Purchased_7() { return &___Purchased_7; }
	inline void set_Purchased_7(String_t* value)
	{
		___Purchased_7 = value;
		Il2CppCodeGenWriteBarrier(&___Purchased_7, value);
	}

	inline static int32_t get_offset_of_Content_8() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___Content_8)); }
	inline String_t* get_Content_8() const { return ___Content_8; }
	inline String_t** get_address_of_Content_8() { return &___Content_8; }
	inline void set_Content_8(String_t* value)
	{
		___Content_8 = value;
		Il2CppCodeGenWriteBarrier(&___Content_8, value);
	}

	inline static int32_t get_offset_of_FirstScore_9() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___FirstScore_9)); }
	inline String_t* get_FirstScore_9() const { return ___FirstScore_9; }
	inline String_t** get_address_of_FirstScore_9() { return &___FirstScore_9; }
	inline void set_FirstScore_9(String_t* value)
	{
		___FirstScore_9 = value;
		Il2CppCodeGenWriteBarrier(&___FirstScore_9, value);
	}

	inline static int32_t get_offset_of_RescanScore_10() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___RescanScore_10)); }
	inline String_t* get_RescanScore_10() const { return ___RescanScore_10; }
	inline String_t** get_address_of_RescanScore_10() { return &___RescanScore_10; }
	inline void set_RescanScore_10(String_t* value)
	{
		___RescanScore_10 = value;
		Il2CppCodeGenWriteBarrier(&___RescanScore_10, value);
	}

	inline static int32_t get_offset_of_SelfFirstScore_11() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___SelfFirstScore_11)); }
	inline String_t* get_SelfFirstScore_11() const { return ___SelfFirstScore_11; }
	inline String_t** get_address_of_SelfFirstScore_11() { return &___SelfFirstScore_11; }
	inline void set_SelfFirstScore_11(String_t* value)
	{
		___SelfFirstScore_11 = value;
		Il2CppCodeGenWriteBarrier(&___SelfFirstScore_11, value);
	}

	inline static int32_t get_offset_of_SelfRescanScore_12() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___SelfRescanScore_12)); }
	inline String_t* get_SelfRescanScore_12() const { return ___SelfRescanScore_12; }
	inline String_t** get_address_of_SelfRescanScore_12() { return &___SelfRescanScore_12; }
	inline void set_SelfRescanScore_12(String_t* value)
	{
		___SelfRescanScore_12 = value;
		Il2CppCodeGenWriteBarrier(&___SelfRescanScore_12, value);
	}

	inline static int32_t get_offset_of_Version_13() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___Version_13)); }
	inline String_t* get_Version_13() const { return ___Version_13; }
	inline String_t** get_address_of_Version_13() { return &___Version_13; }
	inline void set_Version_13(String_t* value)
	{
		___Version_13 = value;
		Il2CppCodeGenWriteBarrier(&___Version_13, value);
	}

	inline static int32_t get_offset_of_ForMonth_14() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___ForMonth_14)); }
	inline String_t* get_ForMonth_14() const { return ___ForMonth_14; }
	inline String_t** get_address_of_ForMonth_14() { return &___ForMonth_14; }
	inline void set_ForMonth_14(String_t* value)
	{
		___ForMonth_14 = value;
		Il2CppCodeGenWriteBarrier(&___ForMonth_14, value);
	}

	inline static int32_t get_offset_of_First_15() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___First_15)); }
	inline String_t* get_First_15() const { return ___First_15; }
	inline String_t** get_address_of_First_15() { return &___First_15; }
	inline void set_First_15(String_t* value)
	{
		___First_15 = value;
		Il2CppCodeGenWriteBarrier(&___First_15, value);
	}

	inline static int32_t get_offset_of_SelfFirst_16() { return static_cast<int32_t>(offsetof(CodeTest_t2096217477, ___SelfFirst_16)); }
	inline String_t* get_SelfFirst_16() const { return ___SelfFirst_16; }
	inline String_t** get_address_of_SelfFirst_16() { return &___SelfFirst_16; }
	inline void set_SelfFirst_16(String_t* value)
	{
		___SelfFirst_16 = value;
		Il2CppCodeGenWriteBarrier(&___SelfFirst_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
