﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataService
struct  DataService_t2602786891  : public Il2CppObject
{
public:
	// SQLite4Unity3d.SQLiteConnection DataService::_connection
	SQLiteConnection_t3529499386 * ____connection_0;

public:
	inline static int32_t get_offset_of__connection_0() { return static_cast<int32_t>(offsetof(DataService_t2602786891, ____connection_0)); }
	inline SQLiteConnection_t3529499386 * get__connection_0() const { return ____connection_0; }
	inline SQLiteConnection_t3529499386 ** get_address_of__connection_0() { return &____connection_0; }
	inline void set__connection_0(SQLiteConnection_t3529499386 * value)
	{
		____connection_0 = value;
		Il2CppCodeGenWriteBarrier(&____connection_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
