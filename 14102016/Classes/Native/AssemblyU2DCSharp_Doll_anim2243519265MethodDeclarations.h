﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Doll_anim
struct Doll_anim_t2243519265;

#include "codegen/il2cpp-codegen.h"

// System.Void Doll_anim::.ctor()
extern "C"  void Doll_anim__ctor_m3874049994 (Doll_anim_t2243519265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Doll_anim::Start()
extern "C"  void Doll_anim_Start_m1352110354 (Doll_anim_t2243519265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Doll_anim::Update()
extern "C"  void Doll_anim_Update_m26071341 (Doll_anim_t2243519265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Doll_anim::dollAnimate()
extern "C"  void Doll_anim_dollAnimate_m255752814 (Doll_anim_t2243519265 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
