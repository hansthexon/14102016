﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1
struct GameManager1_t1783026604;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GameManager1::.ctor()
extern "C"  void GameManager1__ctor_m1915110887 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::Start()
extern "C"  void GameManager1_Start_m3040170219 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::Update()
extern "C"  void GameManager1_Update_m3767203150 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::OpenArCam()
extern "C"  void GameManager1_OpenArCam_m1009638599 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::OpenArPanel()
extern "C"  void GameManager1_OpenArPanel_m3212918404 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::CloseARPanel()
extern "C"  void GameManager1_CloseARPanel_m3233389356 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::ObjectFound(System.String)
extern "C"  void GameManager1_ObjectFound_m3079011056 (GameManager1_t1783026604 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::fetchInfoFromDb(System.String)
extern "C"  void GameManager1_fetchInfoFromDb_m1491247679 (GameManager1_t1783026604 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::SetScore()
extern "C"  void GameManager1_SetScore_m646606725 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::SetPlanetScore()
extern "C"  void GameManager1_SetPlanetScore_m2400110743 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::Setmyscore()
extern "C"  void GameManager1_Setmyscore_m1818355893 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::backButton()
extern "C"  void GameManager1_backButton_m1096079696 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::Replay()
extern "C"  void GameManager1_Replay_m17475868 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::playvideo()
extern "C"  void GameManager1_playvideo_m2660121306 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1::isSelfFirst()
extern "C"  bool GameManager1_isSelfFirst_m148858639 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1::isFirst()
extern "C"  bool GameManager1_isFirst_m3344754127 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::SaveScore(System.Boolean)
extern "C"  void GameManager1_SaveScore_m87911381 (GameManager1_t1783026604 * __this, bool ___isStar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager1::ShowReplay()
extern "C"  Il2CppObject * GameManager1_ShowReplay_m90339401 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::UpdateDisplay()
extern "C"  void GameManager1_UpdateDisplay_m2800516746 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::UpdateBar()
extern "C"  void GameManager1_UpdateBar_m1254324415 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::GetCurrentObjective()
extern "C"  void GameManager1_GetCurrentObjective_m1585222019 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager1::ShowConratsScreen()
extern "C"  Il2CppObject * GameManager1_ShowConratsScreen_m1129066492 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::Congratsback()
extern "C"  void GameManager1_Congratsback_m1752754037 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite GameManager1::getmysprite(System.String)
extern "C"  Sprite_t309593783 * GameManager1_getmysprite_m3584668551 (GameManager1_t1783026604 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::showRoomObjects()
extern "C"  void GameManager1_showRoomObjects_m2362879749 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::playy()
extern "C"  void GameManager1_playy_m692074026 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::SetFive(System.Int32)
extern "C"  void GameManager1_SetFive_m3111646490 (GameManager1_t1783026604 * __this, int32_t ___five0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::close()
extern "C"  void GameManager1_close_m3428011133 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::OpenParentCorner()
extern "C"  void GameManager1_OpenParentCorner_m2065985198 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::closeParemt()
extern "C"  void GameManager1_closeParemt_m1726615020 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::OpenPurchaseScreen()
extern "C"  void GameManager1_OpenPurchaseScreen_m1306900812 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::buyfreescode(System.Boolean)
extern "C"  void GameManager1_buyfreescode_m1725416928 (GameManager1_t1783026604 * __this, bool ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::buyletsthree(System.Boolean)
extern "C"  void GameManager1_buyletsthree_m3881308812 (GameManager1_t1783026604 * __this, bool ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::three_or_singlebuy()
extern "C"  void GameManager1_three_or_singlebuy_m168342516 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1::OpenPolitePlanet()
extern "C"  void GameManager1_OpenPolitePlanet_m405187056 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager1::ploiteloadingscreen()
extern "C"  Il2CppObject * GameManager1_ploiteloadingscreen_m889259654 (GameManager1_t1783026604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
