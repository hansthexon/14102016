﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager2
struct GameManager2_t3349110545;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GameManager2::.ctor()
extern "C"  void GameManager2__ctor_m2145781772 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::Start()
extern "C"  void GameManager2_Start_m2261725628 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::Update()
extern "C"  void GameManager2_Update_m2622670493 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::OpenArCam()
extern "C"  void GameManager2_OpenArCam_m683476790 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::ObjectFound(System.String)
extern "C"  void GameManager2_ObjectFound_m514794197 (GameManager2_t3349110545 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::fetchInfoFromDb(System.String)
extern "C"  void GameManager2_fetchInfoFromDb_m3373077776 (GameManager2_t3349110545 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::SetScore()
extern "C"  void GameManager2_SetScore_m1886873332 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::SetPlanetScore()
extern "C"  void GameManager2_SetPlanetScore_m144269798 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::Setmyscore()
extern "C"  void GameManager2_Setmyscore_m3637798074 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::backButton()
extern "C"  void GameManager2_backButton_m2147894933 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::Replay()
extern "C"  void GameManager2_Replay_m441139499 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::playvideo()
extern "C"  void GameManager2_playvideo_m3545249289 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager2::isSelfFirst()
extern "C"  bool GameManager2_isSelfFirst_m1833184050 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager2::isFirst()
extern "C"  bool GameManager2_isFirst_m3381796332 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::SaveScore(System.Boolean)
extern "C"  void GameManager2_SaveScore_m2282000400 (GameManager2_t3349110545 * __this, bool ___isStar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager2::UpdateDisplay()
extern "C"  void GameManager2_UpdateDisplay_m499389103 (GameManager2_t3349110545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
