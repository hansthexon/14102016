﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LitJson.FactoryFunc`1<System.Object>
struct FactoryFunc_1_t741989655;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1A`1<System.Object>
struct  U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554  : public Il2CppObject
{
public:
	// LitJson.FactoryFunc`1<T> LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1A`1::factory
	FactoryFunc_1_t741989655 * ___factory_0;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(U3CRegisterFactoryU3Ec__AnonStorey1A_1_t3468324554, ___factory_0)); }
	inline FactoryFunc_1_t741989655 * get_factory_0() const { return ___factory_0; }
	inline FactoryFunc_1_t741989655 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(FactoryFunc_1_t741989655 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier(&___factory_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
