﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>
struct Action_2_t3176658245;
// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>
struct Action_2_t3662845984;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.UnityTypeBindings/<Register>c__AnonStorey1B
struct  U3CRegisterU3Ec__AnonStorey1B_t484806456  : public Il2CppObject
{
public:
	// System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter> LitJson.UnityTypeBindings/<Register>c__AnonStorey1B::writeVector2
	Action_2_t3176658245 * ___writeVector2_0;
	// System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter> LitJson.UnityTypeBindings/<Register>c__AnonStorey1B::writeVector3
	Action_2_t3662845984 * ___writeVector3_1;

public:
	inline static int32_t get_offset_of_writeVector2_0() { return static_cast<int32_t>(offsetof(U3CRegisterU3Ec__AnonStorey1B_t484806456, ___writeVector2_0)); }
	inline Action_2_t3176658245 * get_writeVector2_0() const { return ___writeVector2_0; }
	inline Action_2_t3176658245 ** get_address_of_writeVector2_0() { return &___writeVector2_0; }
	inline void set_writeVector2_0(Action_2_t3176658245 * value)
	{
		___writeVector2_0 = value;
		Il2CppCodeGenWriteBarrier(&___writeVector2_0, value);
	}

	inline static int32_t get_offset_of_writeVector3_1() { return static_cast<int32_t>(offsetof(U3CRegisterU3Ec__AnonStorey1B_t484806456, ___writeVector3_1)); }
	inline Action_2_t3662845984 * get_writeVector3_1() const { return ___writeVector3_1; }
	inline Action_2_t3662845984 ** get_address_of_writeVector3_1() { return &___writeVector3_1; }
	inline void set_writeVector3_1(Action_2_t3662845984 * value)
	{
		___writeVector3_1 = value;
		Il2CppCodeGenWriteBarrier(&___writeVector3_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
