﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MedaiPlayerSampleGUI
struct MedaiPlayerSampleGUI_t1330597946;

#include "codegen/il2cpp-codegen.h"

// System.Void MedaiPlayerSampleGUI::.ctor()
extern "C"  void MedaiPlayerSampleGUI__ctor_m2940183793 (MedaiPlayerSampleGUI_t1330597946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleGUI::Start()
extern "C"  void MedaiPlayerSampleGUI_Start_m2822148793 (MedaiPlayerSampleGUI_t1330597946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleGUI::Update()
extern "C"  void MedaiPlayerSampleGUI_Update_m2288046456 (MedaiPlayerSampleGUI_t1330597946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleGUI::OnGUI()
extern "C"  void MedaiPlayerSampleGUI_OnGUI_m1049895987 (MedaiPlayerSampleGUI_t1330597946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleGUI::OnEnd()
extern "C"  void MedaiPlayerSampleGUI_OnEnd_m881103851 (MedaiPlayerSampleGUI_t1330597946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
