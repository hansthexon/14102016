﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// MediaPlayerCtrl/VideoReady
struct VideoReady_t2776178769;
// MediaPlayerCtrl/VideoEnd
struct VideoEnd_t1362782769;
// MediaPlayerCtrl/VideoError
struct VideoError_t491951502;
// MediaPlayerCtrl/VideoFirstFrameReady
struct VideoFirstFrameReady_t3566592450;
// GameManager1
struct GameManager1_t1783026604;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT2051437640.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIA_SCALE2298611602.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MediaPlayerCtrl
struct  MediaPlayerCtrl_t1284484152  : public MonoBehaviour_t1158329972
{
public:
	// System.String MediaPlayerCtrl::m_strFileName
	String_t* ___m_strFileName_2;
	// UnityEngine.GameObject[] MediaPlayerCtrl::m_TargetMaterial
	GameObjectU5BU5D_t3057952154* ___m_TargetMaterial_3;
	// UnityEngine.Texture2D MediaPlayerCtrl::m_VideoTexture
	Texture2D_t3542995729 * ___m_VideoTexture_4;
	// UnityEngine.Texture2D MediaPlayerCtrl::m_VideoTextureDummy
	Texture2D_t3542995729 * ___m_VideoTextureDummy_5;
	// MediaPlayerCtrl/MEDIAPLAYER_STATE MediaPlayerCtrl::m_CurrentState
	int32_t ___m_CurrentState_6;
	// System.Int32 MediaPlayerCtrl::m_iCurrentSeekPosition
	int32_t ___m_iCurrentSeekPosition_7;
	// System.Single MediaPlayerCtrl::m_fVolume
	float ___m_fVolume_8;
	// System.Boolean MediaPlayerCtrl::m_bFullScreen
	bool ___m_bFullScreen_9;
	// System.Boolean MediaPlayerCtrl::m_bSupportRockchip
	bool ___m_bSupportRockchip_10;
	// MediaPlayerCtrl/VideoReady MediaPlayerCtrl::OnReady
	VideoReady_t2776178769 * ___OnReady_11;
	// MediaPlayerCtrl/VideoEnd MediaPlayerCtrl::OnEnd
	VideoEnd_t1362782769 * ___OnEnd_12;
	// MediaPlayerCtrl/VideoError MediaPlayerCtrl::OnVideoError
	VideoError_t491951502 * ___OnVideoError_13;
	// MediaPlayerCtrl/VideoFirstFrameReady MediaPlayerCtrl::OnVideoFirstFrameReady
	VideoFirstFrameReady_t3566592450 * ___OnVideoFirstFrameReady_14;
	// GameManager1 MediaPlayerCtrl::gamemanager
	GameManager1_t1783026604 * ___gamemanager_15;
	// System.Int32 MediaPlayerCtrl::m_iPauseFrame
	int32_t ___m_iPauseFrame_16;
	// System.Int32 MediaPlayerCtrl::m_iAndroidMgrID
	int32_t ___m_iAndroidMgrID_17;
	// System.Boolean MediaPlayerCtrl::m_bIsFirstFrameReady
	bool ___m_bIsFirstFrameReady_18;
	// System.Boolean MediaPlayerCtrl::m_bFirst
	bool ___m_bFirst_19;
	// MediaPlayerCtrl/MEDIA_SCALE MediaPlayerCtrl::m_ScaleValue
	int32_t ___m_ScaleValue_20;
	// UnityEngine.GameObject[] MediaPlayerCtrl::m_objResize
	GameObjectU5BU5D_t3057952154* ___m_objResize_21;
	// System.Boolean MediaPlayerCtrl::m_bLoop
	bool ___m_bLoop_22;
	// System.Boolean MediaPlayerCtrl::m_bAutoPlay
	bool ___m_bAutoPlay_23;
	// System.Boolean MediaPlayerCtrl::m_bStop
	bool ___m_bStop_24;
	// System.Boolean MediaPlayerCtrl::m_bInit
	bool ___m_bInit_25;
	// System.Boolean MediaPlayerCtrl::m_bCheckFBO
	bool ___m_bCheckFBO_26;
	// System.Boolean MediaPlayerCtrl::m_bPause
	bool ___m_bPause_27;
	// System.Int32 MediaPlayerCtrl::m_iID
	int32_t ___m_iID_28;
	// UnityEngine.Texture2D MediaPlayerCtrl::_videoTexture
	Texture2D_t3542995729 * ____videoTexture_29;

public:
	inline static int32_t get_offset_of_m_strFileName_2() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_strFileName_2)); }
	inline String_t* get_m_strFileName_2() const { return ___m_strFileName_2; }
	inline String_t** get_address_of_m_strFileName_2() { return &___m_strFileName_2; }
	inline void set_m_strFileName_2(String_t* value)
	{
		___m_strFileName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_strFileName_2, value);
	}

	inline static int32_t get_offset_of_m_TargetMaterial_3() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_TargetMaterial_3)); }
	inline GameObjectU5BU5D_t3057952154* get_m_TargetMaterial_3() const { return ___m_TargetMaterial_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_TargetMaterial_3() { return &___m_TargetMaterial_3; }
	inline void set_m_TargetMaterial_3(GameObjectU5BU5D_t3057952154* value)
	{
		___m_TargetMaterial_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_TargetMaterial_3, value);
	}

	inline static int32_t get_offset_of_m_VideoTexture_4() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_VideoTexture_4)); }
	inline Texture2D_t3542995729 * get_m_VideoTexture_4() const { return ___m_VideoTexture_4; }
	inline Texture2D_t3542995729 ** get_address_of_m_VideoTexture_4() { return &___m_VideoTexture_4; }
	inline void set_m_VideoTexture_4(Texture2D_t3542995729 * value)
	{
		___m_VideoTexture_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_VideoTexture_4, value);
	}

	inline static int32_t get_offset_of_m_VideoTextureDummy_5() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_VideoTextureDummy_5)); }
	inline Texture2D_t3542995729 * get_m_VideoTextureDummy_5() const { return ___m_VideoTextureDummy_5; }
	inline Texture2D_t3542995729 ** get_address_of_m_VideoTextureDummy_5() { return &___m_VideoTextureDummy_5; }
	inline void set_m_VideoTextureDummy_5(Texture2D_t3542995729 * value)
	{
		___m_VideoTextureDummy_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_VideoTextureDummy_5, value);
	}

	inline static int32_t get_offset_of_m_CurrentState_6() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_CurrentState_6)); }
	inline int32_t get_m_CurrentState_6() const { return ___m_CurrentState_6; }
	inline int32_t* get_address_of_m_CurrentState_6() { return &___m_CurrentState_6; }
	inline void set_m_CurrentState_6(int32_t value)
	{
		___m_CurrentState_6 = value;
	}

	inline static int32_t get_offset_of_m_iCurrentSeekPosition_7() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_iCurrentSeekPosition_7)); }
	inline int32_t get_m_iCurrentSeekPosition_7() const { return ___m_iCurrentSeekPosition_7; }
	inline int32_t* get_address_of_m_iCurrentSeekPosition_7() { return &___m_iCurrentSeekPosition_7; }
	inline void set_m_iCurrentSeekPosition_7(int32_t value)
	{
		___m_iCurrentSeekPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_fVolume_8() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_fVolume_8)); }
	inline float get_m_fVolume_8() const { return ___m_fVolume_8; }
	inline float* get_address_of_m_fVolume_8() { return &___m_fVolume_8; }
	inline void set_m_fVolume_8(float value)
	{
		___m_fVolume_8 = value;
	}

	inline static int32_t get_offset_of_m_bFullScreen_9() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bFullScreen_9)); }
	inline bool get_m_bFullScreen_9() const { return ___m_bFullScreen_9; }
	inline bool* get_address_of_m_bFullScreen_9() { return &___m_bFullScreen_9; }
	inline void set_m_bFullScreen_9(bool value)
	{
		___m_bFullScreen_9 = value;
	}

	inline static int32_t get_offset_of_m_bSupportRockchip_10() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bSupportRockchip_10)); }
	inline bool get_m_bSupportRockchip_10() const { return ___m_bSupportRockchip_10; }
	inline bool* get_address_of_m_bSupportRockchip_10() { return &___m_bSupportRockchip_10; }
	inline void set_m_bSupportRockchip_10(bool value)
	{
		___m_bSupportRockchip_10 = value;
	}

	inline static int32_t get_offset_of_OnReady_11() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnReady_11)); }
	inline VideoReady_t2776178769 * get_OnReady_11() const { return ___OnReady_11; }
	inline VideoReady_t2776178769 ** get_address_of_OnReady_11() { return &___OnReady_11; }
	inline void set_OnReady_11(VideoReady_t2776178769 * value)
	{
		___OnReady_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnReady_11, value);
	}

	inline static int32_t get_offset_of_OnEnd_12() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnEnd_12)); }
	inline VideoEnd_t1362782769 * get_OnEnd_12() const { return ___OnEnd_12; }
	inline VideoEnd_t1362782769 ** get_address_of_OnEnd_12() { return &___OnEnd_12; }
	inline void set_OnEnd_12(VideoEnd_t1362782769 * value)
	{
		___OnEnd_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnEnd_12, value);
	}

	inline static int32_t get_offset_of_OnVideoError_13() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnVideoError_13)); }
	inline VideoError_t491951502 * get_OnVideoError_13() const { return ___OnVideoError_13; }
	inline VideoError_t491951502 ** get_address_of_OnVideoError_13() { return &___OnVideoError_13; }
	inline void set_OnVideoError_13(VideoError_t491951502 * value)
	{
		___OnVideoError_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoError_13, value);
	}

	inline static int32_t get_offset_of_OnVideoFirstFrameReady_14() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___OnVideoFirstFrameReady_14)); }
	inline VideoFirstFrameReady_t3566592450 * get_OnVideoFirstFrameReady_14() const { return ___OnVideoFirstFrameReady_14; }
	inline VideoFirstFrameReady_t3566592450 ** get_address_of_OnVideoFirstFrameReady_14() { return &___OnVideoFirstFrameReady_14; }
	inline void set_OnVideoFirstFrameReady_14(VideoFirstFrameReady_t3566592450 * value)
	{
		___OnVideoFirstFrameReady_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnVideoFirstFrameReady_14, value);
	}

	inline static int32_t get_offset_of_gamemanager_15() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___gamemanager_15)); }
	inline GameManager1_t1783026604 * get_gamemanager_15() const { return ___gamemanager_15; }
	inline GameManager1_t1783026604 ** get_address_of_gamemanager_15() { return &___gamemanager_15; }
	inline void set_gamemanager_15(GameManager1_t1783026604 * value)
	{
		___gamemanager_15 = value;
		Il2CppCodeGenWriteBarrier(&___gamemanager_15, value);
	}

	inline static int32_t get_offset_of_m_iPauseFrame_16() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_iPauseFrame_16)); }
	inline int32_t get_m_iPauseFrame_16() const { return ___m_iPauseFrame_16; }
	inline int32_t* get_address_of_m_iPauseFrame_16() { return &___m_iPauseFrame_16; }
	inline void set_m_iPauseFrame_16(int32_t value)
	{
		___m_iPauseFrame_16 = value;
	}

	inline static int32_t get_offset_of_m_iAndroidMgrID_17() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_iAndroidMgrID_17)); }
	inline int32_t get_m_iAndroidMgrID_17() const { return ___m_iAndroidMgrID_17; }
	inline int32_t* get_address_of_m_iAndroidMgrID_17() { return &___m_iAndroidMgrID_17; }
	inline void set_m_iAndroidMgrID_17(int32_t value)
	{
		___m_iAndroidMgrID_17 = value;
	}

	inline static int32_t get_offset_of_m_bIsFirstFrameReady_18() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bIsFirstFrameReady_18)); }
	inline bool get_m_bIsFirstFrameReady_18() const { return ___m_bIsFirstFrameReady_18; }
	inline bool* get_address_of_m_bIsFirstFrameReady_18() { return &___m_bIsFirstFrameReady_18; }
	inline void set_m_bIsFirstFrameReady_18(bool value)
	{
		___m_bIsFirstFrameReady_18 = value;
	}

	inline static int32_t get_offset_of_m_bFirst_19() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bFirst_19)); }
	inline bool get_m_bFirst_19() const { return ___m_bFirst_19; }
	inline bool* get_address_of_m_bFirst_19() { return &___m_bFirst_19; }
	inline void set_m_bFirst_19(bool value)
	{
		___m_bFirst_19 = value;
	}

	inline static int32_t get_offset_of_m_ScaleValue_20() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_ScaleValue_20)); }
	inline int32_t get_m_ScaleValue_20() const { return ___m_ScaleValue_20; }
	inline int32_t* get_address_of_m_ScaleValue_20() { return &___m_ScaleValue_20; }
	inline void set_m_ScaleValue_20(int32_t value)
	{
		___m_ScaleValue_20 = value;
	}

	inline static int32_t get_offset_of_m_objResize_21() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_objResize_21)); }
	inline GameObjectU5BU5D_t3057952154* get_m_objResize_21() const { return ___m_objResize_21; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_m_objResize_21() { return &___m_objResize_21; }
	inline void set_m_objResize_21(GameObjectU5BU5D_t3057952154* value)
	{
		___m_objResize_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_objResize_21, value);
	}

	inline static int32_t get_offset_of_m_bLoop_22() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bLoop_22)); }
	inline bool get_m_bLoop_22() const { return ___m_bLoop_22; }
	inline bool* get_address_of_m_bLoop_22() { return &___m_bLoop_22; }
	inline void set_m_bLoop_22(bool value)
	{
		___m_bLoop_22 = value;
	}

	inline static int32_t get_offset_of_m_bAutoPlay_23() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bAutoPlay_23)); }
	inline bool get_m_bAutoPlay_23() const { return ___m_bAutoPlay_23; }
	inline bool* get_address_of_m_bAutoPlay_23() { return &___m_bAutoPlay_23; }
	inline void set_m_bAutoPlay_23(bool value)
	{
		___m_bAutoPlay_23 = value;
	}

	inline static int32_t get_offset_of_m_bStop_24() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bStop_24)); }
	inline bool get_m_bStop_24() const { return ___m_bStop_24; }
	inline bool* get_address_of_m_bStop_24() { return &___m_bStop_24; }
	inline void set_m_bStop_24(bool value)
	{
		___m_bStop_24 = value;
	}

	inline static int32_t get_offset_of_m_bInit_25() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bInit_25)); }
	inline bool get_m_bInit_25() const { return ___m_bInit_25; }
	inline bool* get_address_of_m_bInit_25() { return &___m_bInit_25; }
	inline void set_m_bInit_25(bool value)
	{
		___m_bInit_25 = value;
	}

	inline static int32_t get_offset_of_m_bCheckFBO_26() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bCheckFBO_26)); }
	inline bool get_m_bCheckFBO_26() const { return ___m_bCheckFBO_26; }
	inline bool* get_address_of_m_bCheckFBO_26() { return &___m_bCheckFBO_26; }
	inline void set_m_bCheckFBO_26(bool value)
	{
		___m_bCheckFBO_26 = value;
	}

	inline static int32_t get_offset_of_m_bPause_27() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_bPause_27)); }
	inline bool get_m_bPause_27() const { return ___m_bPause_27; }
	inline bool* get_address_of_m_bPause_27() { return &___m_bPause_27; }
	inline void set_m_bPause_27(bool value)
	{
		___m_bPause_27 = value;
	}

	inline static int32_t get_offset_of_m_iID_28() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ___m_iID_28)); }
	inline int32_t get_m_iID_28() const { return ___m_iID_28; }
	inline int32_t* get_address_of_m_iID_28() { return &___m_iID_28; }
	inline void set_m_iID_28(int32_t value)
	{
		___m_iID_28 = value;
	}

	inline static int32_t get_offset_of__videoTexture_29() { return static_cast<int32_t>(offsetof(MediaPlayerCtrl_t1284484152, ____videoTexture_29)); }
	inline Texture2D_t3542995729 * get__videoTexture_29() const { return ____videoTexture_29; }
	inline Texture2D_t3542995729 ** get_address_of__videoTexture_29() { return &____videoTexture_29; }
	inline void set__videoTexture_29(Texture2D_t3542995729 * value)
	{
		____videoTexture_29 = value;
		Il2CppCodeGenWriteBarrier(&____videoTexture_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
