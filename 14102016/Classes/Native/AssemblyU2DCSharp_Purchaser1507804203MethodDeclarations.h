﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Purchaser
struct Purchaser_t1507804203;
// System.String
struct String_t;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t547992434;
// UnityEngine.Purchasing.Product
struct Product_t1203687971;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc2407199463.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purch547992434.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Prod1203687971.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"

// System.Void Purchaser::.ctor()
extern "C"  void Purchaser__ctor_m517928038 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::.cctor()
extern "C"  void Purchaser__cctor_m1580609569 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::Start()
extern "C"  void Purchaser_Start_m459074674 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::InitializePurchasing()
extern "C"  void Purchaser_InitializePurchasing_m4230090190 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Purchaser::IsInitialized()
extern "C"  bool Purchaser_IsInitialized_m1339705182 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuyConsumable()
extern "C"  void Purchaser_BuyConsumable_m1494744739 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuyNonConsumable()
extern "C"  void Purchaser_BuyNonConsumable_m2126781700 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuySubscription()
extern "C"  void Purchaser_BuySubscription_m2604204239 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuySubscription24()
extern "C"  void Purchaser_BuySubscription24_m2822237349 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuySubscription36()
extern "C"  void Purchaser_BuySubscription36_m2539912250 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuyProductID(System.String)
extern "C"  void Purchaser_BuyProductID_m3538403224 (Purchaser_t1507804203 * __this, String_t* ___productId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::RestorePurchases()
extern "C"  void Purchaser_RestorePurchases_m1436325650 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C"  void Purchaser_OnInitialized_m42589188 (Purchaser_t1507804203 * __this, Il2CppObject * ___controller0, Il2CppObject * ___extensions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C"  void Purchaser_OnInitializeFailed_m30504455 (Purchaser_t1507804203 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.PurchaseProcessingResult Purchaser::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C"  int32_t Purchaser_ProcessPurchase_m3487149768 (Purchaser_t1507804203 * __this, PurchaseEventArgs_t547992434 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C"  void Purchaser_OnPurchaseFailed_m2533740411 (Purchaser_t1507804203 * __this, Product_t1203687971 * ___product0, int32_t ___failureReason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Purchaser::Paytoserver(System.String,System.String,System.String)
extern "C"  Il2CppObject * Purchaser_Paytoserver_m1294514284 (Purchaser_t1507804203 * __this, String_t* ___pay0, String_t* ___expriydate1, String_t* ___date2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::BuyIapProduct()
extern "C"  void Purchaser_BuyIapProduct_m1204493599 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::setFreecode()
extern "C"  void Purchaser_setFreecode_m2036931313 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::SetScodevalue()
extern "C"  void Purchaser_SetScodevalue_m302231265 (Purchaser_t1507804203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser::<RestorePurchases>m__0(System.Boolean)
extern "C"  void Purchaser_U3CRestorePurchasesU3Em__0_m1806193218 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
