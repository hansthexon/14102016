﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Robot_anim
struct  Robot_anim_t903812464  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Robot_anim::robotButton
	GameObject_t1756533147 * ___robotButton_2;
	// UnityEngine.Animator Robot_anim::robotAnim
	Animator_t69676727 * ___robotAnim_3;

public:
	inline static int32_t get_offset_of_robotButton_2() { return static_cast<int32_t>(offsetof(Robot_anim_t903812464, ___robotButton_2)); }
	inline GameObject_t1756533147 * get_robotButton_2() const { return ___robotButton_2; }
	inline GameObject_t1756533147 ** get_address_of_robotButton_2() { return &___robotButton_2; }
	inline void set_robotButton_2(GameObject_t1756533147 * value)
	{
		___robotButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___robotButton_2, value);
	}

	inline static int32_t get_offset_of_robotAnim_3() { return static_cast<int32_t>(offsetof(Robot_anim_t903812464, ___robotAnim_3)); }
	inline Animator_t69676727 * get_robotAnim_3() const { return ___robotAnim_3; }
	inline Animator_t69676727 ** get_address_of_robotAnim_3() { return &___robotAnim_3; }
	inline void set_robotAnim_3(Animator_t69676727 * value)
	{
		___robotAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___robotAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
