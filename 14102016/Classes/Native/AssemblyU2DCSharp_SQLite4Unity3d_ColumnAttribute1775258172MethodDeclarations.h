﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.ColumnAttribute
struct ColumnAttribute_t1775258172;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.ColumnAttribute::.ctor(System.String)
extern "C"  void ColumnAttribute__ctor_m4049301387 (ColumnAttribute_t1775258172 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.ColumnAttribute::get_Name()
extern "C"  String_t* ColumnAttribute_get_Name_m2127437706 (ColumnAttribute_t1775258172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.ColumnAttribute::set_Name(System.String)
extern "C"  void ColumnAttribute_set_Name_m2311625185 (ColumnAttribute_t1775258172 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
