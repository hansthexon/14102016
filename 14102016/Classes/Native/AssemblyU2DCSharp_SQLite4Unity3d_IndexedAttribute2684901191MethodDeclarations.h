﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.IndexedAttribute
struct IndexedAttribute_t2684901191;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.IndexedAttribute::.ctor()
extern "C"  void IndexedAttribute__ctor_m3917798814 (IndexedAttribute_t2684901191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.IndexedAttribute::.ctor(System.String,System.Int32)
extern "C"  void IndexedAttribute__ctor_m2803481555 (IndexedAttribute_t2684901191 * __this, String_t* ___name0, int32_t ___order1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.IndexedAttribute::get_Name()
extern "C"  String_t* IndexedAttribute_get_Name_m3091576259 (IndexedAttribute_t2684901191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.IndexedAttribute::set_Name(System.String)
extern "C"  void IndexedAttribute_set_Name_m3191648084 (IndexedAttribute_t2684901191 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.IndexedAttribute::get_Order()
extern "C"  int32_t IndexedAttribute_get_Order_m3761185985 (IndexedAttribute_t2684901191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.IndexedAttribute::set_Order(System.Int32)
extern "C"  void IndexedAttribute_set_Order_m3061186180 (IndexedAttribute_t2684901191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.IndexedAttribute::get_Unique()
extern "C"  bool IndexedAttribute_get_Unique_m1231782876 (IndexedAttribute_t2684901191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.IndexedAttribute::set_Unique(System.Boolean)
extern "C"  void IndexedAttribute_set_Unique_m1050783925 (IndexedAttribute_t2684901191 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
