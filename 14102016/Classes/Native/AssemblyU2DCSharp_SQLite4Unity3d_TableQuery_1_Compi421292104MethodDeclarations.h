﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_Comp4163790932MethodDeclarations.h"

// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<Person>::.ctor()
#define CompileResult__ctor_m1559853084(__this, method) ((  void (*) (CompileResult_t421292104 *, const MethodInfo*))CompileResult__ctor_m3789497005_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1/CompileResult<Person>::get_CommandText()
#define CompileResult_get_CommandText_m2402605544(__this, method) ((  String_t* (*) (CompileResult_t421292104 *, const MethodInfo*))CompileResult_get_CommandText_m2764066139_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<Person>::set_CommandText(System.String)
#define CompileResult_set_CommandText_m3566947419(__this, ___value0, method) ((  void (*) (CompileResult_t421292104 *, String_t*, const MethodInfo*))CompileResult_set_CommandText_m1496493232_gshared)(__this, ___value0, method)
// System.Object SQLite4Unity3d.TableQuery`1/CompileResult<Person>::get_Value()
#define CompileResult_get_Value_m2291919633(__this, method) ((  Il2CppObject * (*) (CompileResult_t421292104 *, const MethodInfo*))CompileResult_get_Value_m2706335512_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<Person>::set_Value(System.Object)
#define CompileResult_set_Value_m3934904546(__this, ___value0, method) ((  void (*) (CompileResult_t421292104 *, Il2CppObject *, const MethodInfo*))CompileResult_set_Value_m420182477_gshared)(__this, ___value0, method)
