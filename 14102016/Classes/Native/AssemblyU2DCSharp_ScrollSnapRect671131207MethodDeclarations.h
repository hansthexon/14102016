﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollSnapRect
struct ScrollSnapRect_t671131207;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void ScrollSnapRect::.ctor()
extern "C"  void ScrollSnapRect__ctor_m1701168686 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::Start()
extern "C"  void ScrollSnapRect_Start_m2358348658 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::Update()
extern "C"  void ScrollSnapRect_Update_m1749523887 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::SetPagePositions()
extern "C"  void ScrollSnapRect_SetPagePositions_m3599836733 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::SetPage(System.Int32)
extern "C"  void ScrollSnapRect_SetPage_m3021924952 (ScrollSnapRect_t671131207 * __this, int32_t ___aPageIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::LerpToPage(System.Int32)
extern "C"  void ScrollSnapRect_LerpToPage_m230650146 (ScrollSnapRect_t671131207 * __this, int32_t ___aPageIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::InitPageSelection()
extern "C"  void ScrollSnapRect_InitPageSelection_m967231863 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::SetPageSelection(System.Int32)
extern "C"  void ScrollSnapRect_SetPageSelection_m1595622890 (ScrollSnapRect_t671131207 * __this, int32_t ___aPageIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::NextScreen()
extern "C"  void ScrollSnapRect_NextScreen_m2942630957 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::PreviousScreen()
extern "C"  void ScrollSnapRect_PreviousScreen_m89532027 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ScrollSnapRect::GetNearestPage()
extern "C"  int32_t ScrollSnapRect_GetNearestPage_m2938451295 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnapRect_OnBeginDrag_m1667263646 (ScrollSnapRect_t671131207 * __this, PointerEventData_t1599784723 * ___aEventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnapRect_OnEndDrag_m1262575042 (ScrollSnapRect_t671131207 * __this, PointerEventData_t1599784723 * ___aEventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ScrollSnapRect_OnDrag_m478059615 (ScrollSnapRect_t671131207 * __this, PointerEventData_t1599784723 * ___aEventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::<Start>m__13()
extern "C"  void ScrollSnapRect_U3CStartU3Em__13_m3129712783 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollSnapRect::<Start>m__14()
extern "C"  void ScrollSnapRect_U3CStartU3Em__14_m1227601458 (ScrollSnapRect_t671131207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
