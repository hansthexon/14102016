﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GameManager1
struct GameManager1_t1783026604;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1779888572;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1082256726  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Vuforia.DefaultTrackableEventHandler::cantrackagain
	bool ___cantrackagain_2;
	// System.Int32 Vuforia.DefaultTrackableEventHandler::i
	int32_t ___i_3;
	// GameManager1 Vuforia.DefaultTrackableEventHandler::gm
	GameManager1_t1783026604 * ___gm_4;
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1779888572 * ___mTrackableBehaviour_5;

public:
	inline static int32_t get_offset_of_cantrackagain_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___cantrackagain_2)); }
	inline bool get_cantrackagain_2() const { return ___cantrackagain_2; }
	inline bool* get_address_of_cantrackagain_2() { return &___cantrackagain_2; }
	inline void set_cantrackagain_2(bool value)
	{
		___cantrackagain_2 = value;
	}

	inline static int32_t get_offset_of_i_3() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___i_3)); }
	inline int32_t get_i_3() const { return ___i_3; }
	inline int32_t* get_address_of_i_3() { return &___i_3; }
	inline void set_i_3(int32_t value)
	{
		___i_3 = value;
	}

	inline static int32_t get_offset_of_gm_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___gm_4)); }
	inline GameManager1_t1783026604 * get_gm_4() const { return ___gm_4; }
	inline GameManager1_t1783026604 ** get_address_of_gm_4() { return &___gm_4; }
	inline void set_gm_4(GameManager1_t1783026604 * value)
	{
		___gm_4 = value;
		Il2CppCodeGenWriteBarrier(&___gm_4, value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1082256726, ___mTrackableBehaviour_5)); }
	inline TrackableBehaviour_t1779888572 * get_mTrackableBehaviour_5() const { return ___mTrackableBehaviour_5; }
	inline TrackableBehaviour_t1779888572 ** get_address_of_mTrackableBehaviour_5() { return &___mTrackableBehaviour_5; }
	inline void set_mTrackableBehaviour_5(TrackableBehaviour_t1779888572 * value)
	{
		___mTrackableBehaviour_5 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
