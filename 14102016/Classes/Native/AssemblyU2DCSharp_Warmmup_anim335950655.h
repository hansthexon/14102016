﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Warmmup_anim
struct  Warmmup_anim_t335950655  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Warmmup_anim::warmmupButton
	GameObject_t1756533147 * ___warmmupButton_2;
	// UnityEngine.Animator Warmmup_anim::warmmupAnim
	Animator_t69676727 * ___warmmupAnim_3;

public:
	inline static int32_t get_offset_of_warmmupButton_2() { return static_cast<int32_t>(offsetof(Warmmup_anim_t335950655, ___warmmupButton_2)); }
	inline GameObject_t1756533147 * get_warmmupButton_2() const { return ___warmmupButton_2; }
	inline GameObject_t1756533147 ** get_address_of_warmmupButton_2() { return &___warmmupButton_2; }
	inline void set_warmmupButton_2(GameObject_t1756533147 * value)
	{
		___warmmupButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___warmmupButton_2, value);
	}

	inline static int32_t get_offset_of_warmmupAnim_3() { return static_cast<int32_t>(offsetof(Warmmup_anim_t335950655, ___warmmupAnim_3)); }
	inline Animator_t69676727 * get_warmmupAnim_3() const { return ___warmmupAnim_3; }
	inline Animator_t69676727 ** get_address_of_warmmupAnim_3() { return &___warmmupAnim_3; }
	inline void set_warmmupAnim_3(Animator_t69676727 * value)
	{
		___warmmupAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___warmmupAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
