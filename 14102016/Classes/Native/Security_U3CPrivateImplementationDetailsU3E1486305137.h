﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "Security_U3CPrivateImplementationDetailsU3E_U24Arr1568637719.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=32 <PrivateImplementationDetails>::$field-59F5BD34B6C013DEACC784F69C67E95150033A84
	U24ArrayTypeU3D32_t1568637719  ___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0)); }
	inline U24ArrayTypeU3D32_t1568637719  get_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0() const { return ___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline U24ArrayTypeU3D32_t1568637719 * get_address_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0() { return &___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline void set_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0(U24ArrayTypeU3D32_t1568637719  value)
	{
		___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
