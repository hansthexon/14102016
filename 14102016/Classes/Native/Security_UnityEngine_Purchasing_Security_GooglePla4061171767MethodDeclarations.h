﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.GooglePlayValidator
struct GooglePlayValidator_t4061171767;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.GooglePlayReceipt
struct GooglePlayReceipt_t2643016893;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.Security.GooglePlayValidator::.ctor(System.Byte[])
extern "C"  void GooglePlayValidator__ctor_m2387657698 (GooglePlayValidator_t4061171767 * __this, ByteU5BU5D_t3397334013* ___rsaKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.GooglePlayReceipt UnityEngine.Purchasing.Security.GooglePlayValidator::Validate(System.String,System.String)
extern "C"  GooglePlayReceipt_t2643016893 * GooglePlayValidator_Validate_m56002928 (GooglePlayValidator_t4061171767 * __this, String_t* ___receipt0, String_t* ___signature1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
