﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidBundleIdException
struct InvalidBundleIdException_t3011288315;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidBundleIdException::.ctor()
extern "C"  void InvalidBundleIdException__ctor_m4044292885 (InvalidBundleIdException_t3011288315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
