﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidTimeFormat
struct InvalidTimeFormat_t3933748955;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidTimeFormat::.ctor()
extern "C"  void InvalidTimeFormat__ctor_m2055199385 (InvalidTimeFormat_t3933748955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
