﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.MissingStoreSecretException
struct MissingStoreSecretException_t3547862692;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.Security.MissingStoreSecretException::.ctor(System.String)
extern "C"  void MissingStoreSecretException__ctor_m800783456 (MissingStoreSecretException_t3547862692 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
