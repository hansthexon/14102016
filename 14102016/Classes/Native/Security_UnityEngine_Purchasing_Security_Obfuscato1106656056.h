﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0
struct  U3CDeObfuscateU3Ec__AnonStorey0_t1106656056  : public Il2CppObject
{
public:
	// System.Int32 UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0::key
	int32_t ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CDeObfuscateU3Ec__AnonStorey0_t1106656056, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
