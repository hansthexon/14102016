﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3430526812MethodDeclarations.h"

// System.Void System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3318092852(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t2770297750 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2725500044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String>::Invoke(T)
#define Func_2_Invoke_m2320250248(__this, ___arg10, method) ((  String_t* (*) (Func_2_t2770297750 *, IndexedColumn_t674159988 , const MethodInfo*))Func_2_Invoke_m3394123854_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1615157349(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t2770297750 *, IndexedColumn_t674159988 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1847833305_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3136980486(__this, ___result0, method) ((  String_t* (*) (Func_2_t2770297750 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m535418092_gshared)(__this, ___result0, method)
