﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ConditionalExpression
struct ConditionalExpression_t4183435840;
// System.Linq.Expressions.Expression
struct Expression_t114864668;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.Expression System.Linq.Expressions.ConditionalExpression::get_Test()
extern "C"  Expression_t114864668 * ConditionalExpression_get_Test_m2921520529 (ConditionalExpression_t4183435840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.ConditionalExpression::get_IfTrue()
extern "C"  Expression_t114864668 * ConditionalExpression_get_IfTrue_m294963626 (ConditionalExpression_t4183435840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.ConditionalExpression::get_IfFalse()
extern "C"  Expression_t114864668 * ConditionalExpression_get_IfFalse_m2686051869 (ConditionalExpression_t4183435840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
