﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.MemberExpression
struct MemberExpression_t1790982958;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.Linq.Expressions.MemberExpression::.ctor(System.Linq.Expressions.Expression,System.Reflection.MemberInfo,System.Type)
extern "C"  void MemberExpression__ctor_m243495264 (MemberExpression_t1790982958 * __this, Expression_t114864668 * ___expression0, MemberInfo_t * ___member1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Linq.Expressions.Expression System.Linq.Expressions.MemberExpression::get_Expression()
extern "C"  Expression_t114864668 * MemberExpression_get_Expression_m1047809307 (MemberExpression_t1790982958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo System.Linq.Expressions.MemberExpression::get_Member()
extern "C"  MemberInfo_t * MemberExpression_get_Member_m3815419971 (MemberExpression_t1790982958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
