﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.MemberInitExpression
struct MemberInitExpression_t137172540;
// System.Linq.Expressions.NewExpression
struct NewExpression_t1045017810;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.MemberBinding>
struct ReadOnlyCollection_1_t3673584233;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.NewExpression System.Linq.Expressions.MemberInitExpression::get_NewExpression()
extern "C"  NewExpression_t1045017810 * MemberInitExpression_get_NewExpression_m1994683253 (MemberInitExpression_t137172540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.MemberBinding> System.Linq.Expressions.MemberInitExpression::get_Bindings()
extern "C"  ReadOnlyCollection_1_t3673584233 * MemberInitExpression_get_Bindings_m4187369865 (MemberInitExpression_t137172540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
