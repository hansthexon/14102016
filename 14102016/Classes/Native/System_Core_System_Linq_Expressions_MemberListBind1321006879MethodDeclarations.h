﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.MemberListBinding
struct MemberListBinding_t1321006879;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ElementInit>
struct ReadOnlyCollection_1_t4083992128;

#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ElementInit> System.Linq.Expressions.MemberListBinding::get_Initializers()
extern "C"  ReadOnlyCollection_1_t4083992128 * MemberListBinding_get_Initializers_m1230798794 (MemberListBinding_t1321006879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
