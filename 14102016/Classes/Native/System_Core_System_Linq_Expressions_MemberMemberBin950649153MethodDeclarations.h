﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.MemberMemberBinding
struct MemberMemberBinding_t950649153;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.MemberBinding>
struct ReadOnlyCollection_1_t3673584233;

#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.MemberBinding> System.Linq.Expressions.MemberMemberBinding::get_Bindings()
extern "C"  ReadOnlyCollection_1_t3673584233 * MemberMemberBinding_get_Bindings_m2226571324 (MemberMemberBinding_t950649153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
