﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t2376585677;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t967983361;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1199491699.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C"  void LinkedList_1__ctor_m2999180666_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m2999180666(__this, method) ((  void (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1__ctor_m2999180666_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1__ctor_m2235619199_gshared (LinkedList_1_t2376585677 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define LinkedList_1__ctor_m2235619199(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2376585677 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))LinkedList_1__ctor_m2235619199_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1241496853_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1241496853(__this, ___value0, method) ((  void (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1241496853_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m4084947380_gshared (LinkedList_1_t2376585677 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m4084947380(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2376585677 *, Il2CppArray *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m4084947380_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1850278238_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1850278238(__this, method) ((  Il2CppObject* (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1850278238_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m437131773_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m437131773(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m437131773_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3216615461_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3216615461(__this, method) ((  bool (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3216615461_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m1115541056_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m1115541056(__this, method) ((  bool (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m1115541056_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3124823866_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3124823866(__this, method) ((  Il2CppObject * (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m3124823866_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_VerifyReferencedNode_m4026260407_gshared (LinkedList_1_t2376585677 * __this, LinkedListNode_1_t967983361 * ___node0, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m4026260407(__this, ___node0, method) ((  void (*) (LinkedList_1_t2376585677 *, LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m4026260407_gshared)(__this, ___node0, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C"  LinkedListNode_1_t967983361 * LinkedList_1_AddLast_m1405237197_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_AddLast_m1405237197(__this, ___value0, method) ((  LinkedListNode_1_t967983361 * (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m1405237197_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C"  void LinkedList_1_Clear_m642487645_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m642487645(__this, method) ((  void (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_Clear_m642487645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m672313738_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_Contains_m672313738(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m672313738_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void LinkedList_1_CopyTo_m3638740377_gshared (LinkedList_1_t2376585677 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method);
#define LinkedList_1_CopyTo_m3638740377(__this, ___array0, ___index1, method) ((  void (*) (LinkedList_1_t2376585677 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m3638740377_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C"  LinkedListNode_1_t967983361 * LinkedList_1_Find_m1681505513_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_Find_m1681505513(__this, ___value0, method) ((  LinkedListNode_1_t967983361 * (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))LinkedList_1_Find_m1681505513_gshared)(__this, ___value0, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1199491699  LinkedList_1_GetEnumerator_m3906189317_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m3906189317(__this, method) ((  Enumerator_t1199491699  (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_GetEnumerator_m3906189317_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1_GetObjectData_m3163302640_gshared (LinkedList_1_t2376585677 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m3163302640(__this, ___info0, ___context1, method) ((  void (*) (LinkedList_1_t2376585677 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))LinkedList_1_GetObjectData_m3163302640_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C"  void LinkedList_1_OnDeserialization_m3795794854_gshared (LinkedList_1_t2376585677 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m3795794854(__this, ___sender0, method) ((  void (*) (LinkedList_1_t2376585677 *, Il2CppObject *, const MethodInfo*))LinkedList_1_OnDeserialization_m3795794854_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m2841776017_gshared (LinkedList_1_t2376585677 * __this, int32_t ___value0, const MethodInfo* method);
#define LinkedList_1_Remove_m2841776017(__this, ___value0, method) ((  bool (*) (LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m2841776017_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m2349142508_gshared (LinkedList_1_t2376585677 * __this, LinkedListNode_1_t967983361 * ___node0, const MethodInfo* method);
#define LinkedList_1_Remove_m2349142508(__this, ___node0, method) ((  void (*) (LinkedList_1_t2376585677 *, LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedList_1_Remove_m2349142508_gshared)(__this, ___node0, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m158819622_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m158819622(__this, method) ((  void (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_RemoveLast_m158819622_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m212295514_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m212295514(__this, method) ((  int32_t (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_get_Count_m212295514_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C"  LinkedListNode_1_t967983361 * LinkedList_1_get_First_m278223248_gshared (LinkedList_1_t2376585677 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m278223248(__this, method) ((  LinkedListNode_1_t967983361 * (*) (LinkedList_1_t2376585677 *, const MethodInfo*))LinkedList_1_get_First_m278223248_gshared)(__this, method)
