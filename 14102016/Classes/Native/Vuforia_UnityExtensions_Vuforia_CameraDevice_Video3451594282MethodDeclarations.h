﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.CameraDevice/VideoModeData
struct VideoModeData_t3451594282;
struct VideoModeData_t3451594282_marshaled_pinvoke;
struct VideoModeData_t3451594282_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct VideoModeData_t3451594282;
struct VideoModeData_t3451594282_marshaled_pinvoke;

extern "C" void VideoModeData_t3451594282_marshal_pinvoke(const VideoModeData_t3451594282& unmarshaled, VideoModeData_t3451594282_marshaled_pinvoke& marshaled);
extern "C" void VideoModeData_t3451594282_marshal_pinvoke_back(const VideoModeData_t3451594282_marshaled_pinvoke& marshaled, VideoModeData_t3451594282& unmarshaled);
extern "C" void VideoModeData_t3451594282_marshal_pinvoke_cleanup(VideoModeData_t3451594282_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VideoModeData_t3451594282;
struct VideoModeData_t3451594282_marshaled_com;

extern "C" void VideoModeData_t3451594282_marshal_com(const VideoModeData_t3451594282& unmarshaled, VideoModeData_t3451594282_marshaled_com& marshaled);
extern "C" void VideoModeData_t3451594282_marshal_com_back(const VideoModeData_t3451594282_marshaled_com& marshaled, VideoModeData_t3451594282& unmarshaled);
extern "C" void VideoModeData_t3451594282_marshal_com_cleanup(VideoModeData_t3451594282_marshaled_com& marshaled);
