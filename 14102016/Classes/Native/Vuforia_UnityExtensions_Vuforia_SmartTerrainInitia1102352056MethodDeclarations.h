﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SmartTerrainInitializationInfo
struct SmartTerrainInitializationInfo_t1102352056;
struct SmartTerrainInitializationInfo_t1102352056_marshaled_pinvoke;
struct SmartTerrainInitializationInfo_t1102352056_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SmartTerrainInitializationInfo_t1102352056;
struct SmartTerrainInitializationInfo_t1102352056_marshaled_pinvoke;

extern "C" void SmartTerrainInitializationInfo_t1102352056_marshal_pinvoke(const SmartTerrainInitializationInfo_t1102352056& unmarshaled, SmartTerrainInitializationInfo_t1102352056_marshaled_pinvoke& marshaled);
extern "C" void SmartTerrainInitializationInfo_t1102352056_marshal_pinvoke_back(const SmartTerrainInitializationInfo_t1102352056_marshaled_pinvoke& marshaled, SmartTerrainInitializationInfo_t1102352056& unmarshaled);
extern "C" void SmartTerrainInitializationInfo_t1102352056_marshal_pinvoke_cleanup(SmartTerrainInitializationInfo_t1102352056_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SmartTerrainInitializationInfo_t1102352056;
struct SmartTerrainInitializationInfo_t1102352056_marshaled_com;

extern "C" void SmartTerrainInitializationInfo_t1102352056_marshal_com(const SmartTerrainInitializationInfo_t1102352056& unmarshaled, SmartTerrainInitializationInfo_t1102352056_marshaled_com& marshaled);
extern "C" void SmartTerrainInitializationInfo_t1102352056_marshal_com_back(const SmartTerrainInitializationInfo_t1102352056_marshaled_com& marshaled, SmartTerrainInitializationInfo_t1102352056& unmarshaled);
extern "C" void SmartTerrainInitializationInfo_t1102352056_marshal_com_cleanup(SmartTerrainInitializationInfo_t1102352056_marshaled_com& marshaled);
