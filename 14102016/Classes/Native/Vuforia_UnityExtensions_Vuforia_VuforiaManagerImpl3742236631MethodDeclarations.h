﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaManagerImpl/WordResultData
struct WordResultData_t3742236631;
struct WordResultData_t3742236631_marshaled_pinvoke;
struct WordResultData_t3742236631_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct WordResultData_t3742236631;
struct WordResultData_t3742236631_marshaled_pinvoke;

extern "C" void WordResultData_t3742236631_marshal_pinvoke(const WordResultData_t3742236631& unmarshaled, WordResultData_t3742236631_marshaled_pinvoke& marshaled);
extern "C" void WordResultData_t3742236631_marshal_pinvoke_back(const WordResultData_t3742236631_marshaled_pinvoke& marshaled, WordResultData_t3742236631& unmarshaled);
extern "C" void WordResultData_t3742236631_marshal_pinvoke_cleanup(WordResultData_t3742236631_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct WordResultData_t3742236631;
struct WordResultData_t3742236631_marshaled_com;

extern "C" void WordResultData_t3742236631_marshal_com(const WordResultData_t3742236631& unmarshaled, WordResultData_t3742236631_marshaled_com& marshaled);
extern "C" void WordResultData_t3742236631_marshal_com_back(const WordResultData_t3742236631_marshaled_com& marshaled, WordResultData_t3742236631& unmarshaled);
extern "C" void WordResultData_t3742236631_marshal_com_cleanup(WordResultData_t3742236631_marshaled_com& marshaled);
