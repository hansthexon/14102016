﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.WebCamProfile/ProfileData
struct ProfileData_t1724666488;
struct ProfileData_t1724666488_marshaled_pinvoke;
struct ProfileData_t1724666488_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ProfileData_t1724666488;
struct ProfileData_t1724666488_marshaled_pinvoke;

extern "C" void ProfileData_t1724666488_marshal_pinvoke(const ProfileData_t1724666488& unmarshaled, ProfileData_t1724666488_marshaled_pinvoke& marshaled);
extern "C" void ProfileData_t1724666488_marshal_pinvoke_back(const ProfileData_t1724666488_marshaled_pinvoke& marshaled, ProfileData_t1724666488& unmarshaled);
extern "C" void ProfileData_t1724666488_marshal_pinvoke_cleanup(ProfileData_t1724666488_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ProfileData_t1724666488;
struct ProfileData_t1724666488_marshaled_com;

extern "C" void ProfileData_t1724666488_marshal_com(const ProfileData_t1724666488& unmarshaled, ProfileData_t1724666488_marshaled_com& marshaled);
extern "C" void ProfileData_t1724666488_marshal_com_back(const ProfileData_t1724666488_marshaled_com& marshaled, ProfileData_t1724666488& unmarshaled);
extern "C" void ProfileData_t1724666488_marshal_com_cleanup(ProfileData_t1724666488_marshaled_com& marshaled);
