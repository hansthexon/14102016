﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ShimEnumerator_t1060478430;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1884303295_gshared (ShimEnumerator_t1060478430 * __this, Dictionary_2_t955353609 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1884303295(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1060478430 *, Dictionary_2_t955353609 *, const MethodInfo*))ShimEnumerator__ctor_m1884303295_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3652267434_gshared (ShimEnumerator_t1060478430 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3652267434(__this, method) ((  bool (*) (ShimEnumerator_t1060478430 *, const MethodInfo*))ShimEnumerator_MoveNext_m3652267434_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m747130986_gshared (ShimEnumerator_t1060478430 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m747130986(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1060478430 *, const MethodInfo*))ShimEnumerator_get_Entry_m747130986_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1280618911_gshared (ShimEnumerator_t1060478430 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1280618911(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1060478430 *, const MethodInfo*))ShimEnumerator_get_Key_m1280618911_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1812973559_gshared (ShimEnumerator_t1060478430 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1812973559(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1060478430 *, const MethodInfo*))ShimEnumerator_get_Value_m1812973559_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1455263257_gshared (ShimEnumerator_t1060478430 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1455263257(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1060478430 *, const MethodInfo*))ShimEnumerator_get_Current_m1455263257_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C"  void ShimEnumerator_Reset_m2090392005_gshared (ShimEnumerator_t1060478430 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2090392005(__this, method) ((  void (*) (ShimEnumerator_t1060478430 *, const MethodInfo*))ShimEnumerator_Reset_m2090392005_gshared)(__this, method)
