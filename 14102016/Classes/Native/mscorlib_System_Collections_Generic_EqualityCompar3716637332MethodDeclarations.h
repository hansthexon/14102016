﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct EqualityComparer_1_t3716637332;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3831128096_gshared (EqualityComparer_1_t3716637332 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3831128096(__this, method) ((  void (*) (EqualityComparer_1_t3716637332 *, const MethodInfo*))EqualityComparer_1__ctor_m3831128096_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m116568481_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m116568481(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m116568481_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m828463193_gshared (EqualityComparer_1_t3716637332 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m828463193(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3716637332 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m828463193_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m837982799_gshared (EqualityComparer_1_t3716637332 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m837982799(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3716637332 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m837982799_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Default()
extern "C"  EqualityComparer_1_t3716637332 * EqualityComparer_1_get_Default_m1896302964_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1896302964(__this /* static, unused */, method) ((  EqualityComparer_1_t3716637332 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1896302964_gshared)(__this /* static, unused */, method)
