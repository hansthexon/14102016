﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.FactoryFunc>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2766549203(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2131091675 *, Type_t *, FactoryFunc_t2436388556 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.FactoryFunc>::get_Key()
#define KeyValuePair_2_get_Key_m3717829801(__this, method) ((  Type_t * (*) (KeyValuePair_2_t2131091675 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.FactoryFunc>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m618970356(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2131091675 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.FactoryFunc>::get_Value()
#define KeyValuePair_2_get_Value_m190766473(__this, method) ((  FactoryFunc_t2436388556 * (*) (KeyValuePair_2_t2131091675 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.FactoryFunc>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2048443716(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2131091675 *, FactoryFunc_t2436388556 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,LitJson.FactoryFunc>::ToString()
#define KeyValuePair_2_ToString_m1440176588(__this, method) ((  String_t* (*) (KeyValuePair_2_t2131091675 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
