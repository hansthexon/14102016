﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m950010662_gshared (KeyValuePair_2_t2492407411 * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m950010662(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2492407411 *, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))KeyValuePair_2__ctor_m950010662_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3094063940_gshared (KeyValuePair_2_t2492407411 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3094063940(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2492407411 *, const MethodInfo*))KeyValuePair_2_get_Key_m3094063940_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2868193553_gshared (KeyValuePair_2_t2492407411 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2868193553(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2492407411 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2868193553_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Value()
extern "C"  IndexInfo_t848034765  KeyValuePair_2_get_Value_m1745261316_gshared (KeyValuePair_2_t2492407411 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1745261316(__this, method) ((  IndexInfo_t848034765  (*) (KeyValuePair_2_t2492407411 *, const MethodInfo*))KeyValuePair_2_get_Value_m1745261316_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2932767089_gshared (KeyValuePair_2_t2492407411 * __this, IndexInfo_t848034765  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2932767089(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2492407411 *, IndexInfo_t848034765 , const MethodInfo*))KeyValuePair_2_set_Value_m2932767089_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3738640143_gshared (KeyValuePair_2_t2492407411 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3738640143(__this, method) ((  String_t* (*) (KeyValuePair_2_t2492407411 *, const MethodInfo*))KeyValuePair_2_ToString_m3738640143_gshared)(__this, method)
