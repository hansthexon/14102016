﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_t3085371226;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2620100900.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3896932057_gshared (Enumerator_t2620100900 * __this, List_1_t3085371226 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3896932057(__this, ___l0, method) ((  void (*) (Enumerator_t2620100900 *, List_1_t3085371226 *, const MethodInfo*))Enumerator__ctor_m3896932057_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4116876945_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4116876945(__this, method) ((  void (*) (Enumerator_t2620100900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4116876945_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2417842817(__this, method) ((  Il2CppObject * (*) (Enumerator_t2620100900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m643661990_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m643661990(__this, method) ((  void (*) (Enumerator_t2620100900 *, const MethodInfo*))Enumerator_Dispose_m643661990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m570381259_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m570381259(__this, method) ((  void (*) (Enumerator_t2620100900 *, const MethodInfo*))Enumerator_VerifyState_m570381259_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1558544689_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1558544689(__this, method) ((  bool (*) (Enumerator_t2620100900 *, const MethodInfo*))Enumerator_MoveNext_m1558544689_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m3902255900_gshared (Enumerator_t2620100900 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3902255900(__this, method) ((  KeyValuePair_2_t3716250094  (*) (Enumerator_t2620100900 *, const MethodInfo*))Enumerator_get_Current_m3902255900_gshared)(__this, method)
