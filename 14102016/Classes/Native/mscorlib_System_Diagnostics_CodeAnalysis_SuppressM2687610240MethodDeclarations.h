﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
struct SuppressMessageAttribute_t2687610240;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::.ctor(System.String,System.String)
extern "C"  void SuppressMessageAttribute__ctor_m366696946 (SuppressMessageAttribute_t2687610240 * __this, String_t* ___category0, String_t* ___checkId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.CodeAnalysis.SuppressMessageAttribute::set_Justification(System.String)
extern "C"  void SuppressMessageAttribute_set_Justification_m1870207185 (SuppressMessageAttribute_t2687610240 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
