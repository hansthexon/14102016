﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C"  int32_t Interlocked_CompareExchange_m3339239614 (Il2CppObject * __this /* static, unused */, int32_t* ___location10, int32_t ___value1, int32_t ___comparand2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Decrement(System.Int32&)
extern "C"  int32_t Interlocked_Decrement_m70525859 (Il2CppObject * __this /* static, unused */, int32_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Increment(System.Int32&)
extern "C"  int32_t Interlocked_Increment_m129308425 (Il2CppObject * __this /* static, unused */, int32_t* ___location0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::Exchange(System.Int32&,System.Int32)
extern "C"  int32_t Interlocked_Exchange_m4103465028 (Il2CppObject * __this /* static, unused */, int32_t* ___location10, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
